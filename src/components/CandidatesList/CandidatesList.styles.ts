import styled from "styled-components";

export const CandidatesListTable = styled.table`
  width: 100%;
  border-spacing: 0;

  td {
    color: ${({ theme }) => theme.colorFont};
    padding: 16px 8px;
    border-bottom: 1px solid ${({ theme }) => theme.colorDarkGray};
  }

  tr {
  }
`;

export const CandidatesListTableHeader = styled.thead`
  display: table;
`;
