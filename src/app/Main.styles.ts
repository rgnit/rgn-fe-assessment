import styled from "styled-components";
import { navBarWidth } from "@/components/Navbar/Navbar.styles";

export const Main = styled.main`
  padding: 48px;
  left: ${navBarWidth}px;
  position: absolute;
`;

export const Body = styled.body`
  display: flex;
`;
