import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { renderWithRandstadTheme } from "@/lib/test-utils";
import { CandidatesList } from "@/components/CandidatesList/CandidatesList";

describe("CandidatesList", () => {
  it("should have at least one entry", async () => {
    renderWithRandstadTheme(<CandidatesList skillLevelFilter={"senior"} />);
    const row = await screen.findAllByRole("row");
    expect(row.length).toBeGreaterThan(2);
  });
});
