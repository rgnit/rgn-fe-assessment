export type Vacancy = {
  title: string;
  location: {
    city: string;
    country: string;
  };
  experience_level: string;
  salary_indication: string;
  hours_per_week: number;
  starting_date: number; // Unix timestamp in seconds
  description: string;
};
