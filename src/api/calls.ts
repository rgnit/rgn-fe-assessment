import { Vacancy } from "@/types/Vacancy";
import { Candidate } from "@/types/Candidate";

export async function getVacancies(): Promise<Array<Vacancy>> {
  const res = await fetch("http://localhost:3001/vacancies");

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export async function getCandidates(): Promise<Array<Candidate>> {
  const res = await fetch("http://localhost:3001/candidates");

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return res.json();
}
