import { Theme } from "./DefaultTheme";

export const RandstadTheme: Theme = {
  colorPrimary: "#2175d9",
  colorWhite: "#ffffff",
  colorLightGray: "#f5f5f7",
  colorDarkGray: "#d2d2d2",
  colorFontNavbar: "#7a7d90",
  colorFontNavbarHover: "#010101",
  colorHightlightBackground: "#faeee3",
  colorHighlightText: "#CD6A3E",
  colorFont: "#010101",
};
