"use client";
import { getRandomVacancy } from "@/lib/vacancies";
import { useEffect, useState } from "react";
import { Vacancy } from "@/types/Vacancy";
import * as Styled from "./VacancyHightlight.styles";
import { FiStar } from "react-icons/fi";
import { DateTime } from "luxon";
import { ClipLoader } from "react-spinners";
import { useTheme } from "styled-components";

export const VacancyHighlight = () => {
  const [vacancy, setVacancy] = useState<null | Vacancy>(null);
  const theme = useTheme();

  useEffect(() => {
    (async () => {
      setVacancy(await getRandomVacancy());
    })();
  }, []);

  if (vacancy === null) {
    return <ClipLoader color={theme.colorPrimary} />;
  }

  return (
    <Styled.VacancyHighlightContainer>
      <Styled.VacancyHighlightHeader>
        <FiStar /> Vacancy of the day
      </Styled.VacancyHighlightHeader>
      <Styled.VacancyHighlightContent>
        <h2>{vacancy.title}</h2>
        <Styled.VacancyHightlightLocation>
          {vacancy.location.city}, {vacancy.location.country}
        </Styled.VacancyHightlightLocation>
        <Styled.VacancyHightlightDescription>
          {vacancy.description}
        </Styled.VacancyHightlightDescription>
      </Styled.VacancyHighlightContent>
      <Styled.VacancyHightlightMeta>
        <Styled.VacancyHighlightMetaKey>
          Experience level
        </Styled.VacancyHighlightMetaKey>
        <div>{vacancy.experience_level}</div>
        <Styled.VacancyHighlightMetaKey>
          Salary indication
        </Styled.VacancyHighlightMetaKey>
        <div>{vacancy.salary_indication}</div>
        <Styled.VacancyHighlightMetaKey>
          Hours per week
        </Styled.VacancyHighlightMetaKey>
        <div>{vacancy.hours_per_week}</div>
        <Styled.VacancyHighlightMetaKey>
          Starting date
        </Styled.VacancyHighlightMetaKey>
        <div>
          {DateTime.fromMillis(vacancy.starting_date).toFormat("dd LLLL yyyy")}
        </div>
      </Styled.VacancyHightlightMeta>
    </Styled.VacancyHighlightContainer>
  );
};
