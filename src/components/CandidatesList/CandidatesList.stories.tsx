import type { Meta, StoryObj } from "@storybook/react";
import { CandidatesList } from "./CandidatesList";

const meta: Meta<typeof CandidatesList> = {
  component: CandidatesList,
};

export default meta;
type Story = StoryObj<typeof CandidatesList>;

export const Primary: Story = {
  render: () => <CandidatesList skillLevelFilter={"any"} />,
};
