# Welcome to the Randstad Candidate Manager
Hello there! And welcome to our coding challenge. At Randstad you'll be working on applications that are already being used by lots of users. So this challenge reflects our day-to-day job as a Front-End developer. Working on existing bugs and implementing new features.

## How te challenge works
It doesn't matter if you are applying for a junior, medior or senior position, everyone gets this same challenge. In [the issue tracker](https://gitlab.com/rgnit/rgn-fe-assessment/-/issues) of this repository you'll find bug reports and feature requests. Although not labeled as such, there are different complexity levels of the issues. So you don't have to solve them all (!!). Please make a selection of what you think showcases your hard skills, and try to solve those.

## Randstad Candidate Manager
For this challenge you'll be working on our new (fictive) candidate manager. It's a very Work In Progress application, so that's why we need you! The application will help our colleagues at the office to find vacancies and possible candidates.

## How to start
First, clone the repository so you have it locally. Then add it to your own GIT provider (e.g. Gitlab, Github etc.). Please set your personal repo to private, and invite your technical contact for the repo once you are done! If you have a Gitlab account, you can also fork this repo.

### Get it to run locally
You need two terminal windows for this.
In the first terminal you run: `yarn dev` to start the application
In the second terminal you run `yarn database` to start the mock API

## Good luck!
And if you have any questions in the process, feel free to contact your (technical) contact person
