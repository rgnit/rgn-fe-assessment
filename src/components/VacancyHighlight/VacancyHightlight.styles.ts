import styled, { css } from "styled-components";

const borderRadius = "10px";
export const VacancyHighlightContainer = styled.div`
  border: 1px solid ${({ theme }) => theme.colorDarkGray};
  border-radius: ${borderRadius};
  width: 500px;
  margin-top: 32px;
`;

export const VacancyHighlightHeader = styled.div`
  background-color: ${({ theme }) => theme.colorHightlightBackground};
  color: ${({ theme }) => theme.colorHighlightText};
  border-top-left-radius: ${borderRadius};
  border-top-right-radius: ${borderRadius};
  padding: 16px;
  display: flex;
  gap: 8px;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colorDarkGray};
`;

export const VacancyHighlightContent = styled.div`
  padding: 16px;
`;

const VacancyHighlightLightText = css`
  color: ${({ theme }) => theme.colorDarkGray};
`;

export const VacancyHightlightLocation = styled.span`
  ${VacancyHighlightLightText}
`;

export const VacancyHightlightDescription = styled.div`
  margin-top: 16px;
`;

export const VacancyHightlightMeta = styled.div`
  padding: 16px;
  display: grid;
  grid-template-columns: 1fr 1fr;
`;
export const VacancyHightlightMetaEntry = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const VacancyHighlightMetaKey = styled.span`
  ${VacancyHighlightLightText}
`;
