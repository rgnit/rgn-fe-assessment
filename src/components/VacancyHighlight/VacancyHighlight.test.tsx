import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { renderWithRandstadTheme } from "@/lib/test-utils";
import { VacancyHighlight } from "@/components/VacancyHighlight/VacancyHighlight";

describe("VacancyHighlight", () => {
  it("renders a vacancy", async () => {
    renderWithRandstadTheme(<VacancyHighlight />);
    const text = await screen.findByText("Experience level");
    expect(text).toHaveTextContent("Experience level");
  });
});
