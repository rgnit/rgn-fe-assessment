import React from "react";
import { Candidate } from "@/types/Candidate";

type CandidatesFiltersProps = {
  setSkillLevelFilter: (skillLevel: Candidate["skill_level"] | "any") => void;
};
export const CandidatesFilters = ({
  setSkillLevelFilter,
}: CandidatesFiltersProps) => {
  const skillOptions: Array<Candidate["skill_level"] | "any"> = [
    "any",
    "junior",
    "medior",
    "senior",
  ];

  return (
    <div>
      Level:
      <select
        onChange={(event) => {
          setSkillLevelFilter(
            event.target.value as Candidate["skill_level"] | "any",
          );
        }}
      >
        {skillOptions.map((skillOption, index) => (
          <option
            value={skillOption}
            onSelect={(event) => {
              console.log("I changed", event);
            }}
          >
            {skillOption}
          </option>
        ))}
      </select>
    </div>
  );
};
