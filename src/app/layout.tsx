import type { Metadata } from "next";

import { Navbar } from "@/components/Navbar/Navbar";
import StyledComponentsRegistry from "@/lib/styled-components-registry";
import "./global.css";
import { ThemeProvider } from "styled-components";
import { RandstadTheme } from "@/themes/RandstadTheme";
import { Main } from "@/app/Main";

export const metadata: Metadata = {
  title: "Randstad Candidate Manager",
  description: "FE hiring assignment",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <Main>{children}</Main>
    </html>
  );
}
