"use client";
import { VacancyHighlight } from "@/components/VacancyHighlight/VacancyHighlight";

export default function Home() {
  return (
    <div>
      <h1>Dashboard</h1>
      <VacancyHighlight />
    </div>
  );
}
