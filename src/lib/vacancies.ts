import { Vacancy } from "@/types/Vacancy";
import { getVacancies } from "@/api/calls";

export const getRandomVacancy = async (): Promise<Vacancy> => {
  const vacancies = await getVacancies();
  return vacancies[Math.round(Math.random() * vacancies.length)];
};
