"use client";
import React from "react";
import { ThemeProvider } from "styled-components";
import { Inter } from "next/font/google";
import { RandstadTheme } from "@/themes/RandstadTheme";
import { Navbar } from "@/components/Navbar/Navbar";
import StyledComponentsRegistry from "@/lib/styled-components-registry";
import * as Styled from "./Main.styles";

const inter = Inter({ subsets: ["latin"] });

interface MainProps {
  children: React.ReactNode;
}
export const Main = ({ children }: MainProps) => {
  return (
    <StyledComponentsRegistry>
      <ThemeProvider theme={RandstadTheme}>
        <Styled.Body className={inter.className}>
          <nav>
            <Navbar />
          </nav>
          <Styled.Main>{children}</Styled.Main>
        </Styled.Body>
      </ThemeProvider>
    </StyledComponentsRegistry>
  );
};
