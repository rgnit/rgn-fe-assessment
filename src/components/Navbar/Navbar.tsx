"use client";
import React from "react";
import Image from "next/image";
import * as Styled from "./Navbar.styles";
import { FiFileText, FiHome, FiUsers } from "react-icons/fi";
import Link from "next/link";

export const Navbar = () => {
  return (
    <Styled.Navbar data-testid="testNavbar">
      <Styled.LogoContainer>
        <Image
          src="/randstad.svg"
          alt="Randstad Logo"
          width={150}
          height={36}
          priority
        />
      </Styled.LogoContainer>

      <Styled.LinkContainer>
        <Link href={"/"}>
          <FiHome /> Dashboard
        </Link>
        <Link href={"/candidates"}>
          <FiUsers /> Candidates
        </Link>
        <Link href={"/vacancies"}>
          <FiFileText /> Vacancies
        </Link>
      </Styled.LinkContainer>
    </Styled.Navbar>
  );
};
