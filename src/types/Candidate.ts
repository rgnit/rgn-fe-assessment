export type Candidate = {
  first_name: string;
  last_name: string;
  date_of_birth: number;
  skill_level: "junior" | "medior" | "senior";
  field_of_expertise: string;
  city: string;
  country: string;
  preferred_hours_per_week: number;
};
