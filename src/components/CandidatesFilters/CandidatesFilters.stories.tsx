import type { Meta, StoryObj } from "@storybook/react";
import { CandidatesFilters } from "./CandidatesFilters";

const meta: Meta<typeof CandidatesFilters> = {
  component: CandidatesFilters,
};

export default meta;
type Story = StoryObj<typeof CandidatesFilters>;

export const Primary: Story = {
  render: () => <CandidatesFilters setSkillLevelFilter={(level) => {}} />,
};
