import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Home from "@/app/page";
import { renderWithRandstadTheme } from "@/lib/test-utils";
import Page from "@/app/candidates/page";

jest.mock("../../components/CandidatesList/CandidatesList", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />;
    },
    CandidatesList: () => {
      return <div />;
    },
  };
});
describe("CandidatesPage", () => {
  it("renders a heading", () => {
    renderWithRandstadTheme(<Page />);

    const heading = screen.getByRole("heading", {
      name: /Candidates/i,
    });

    expect(heading).toBeInTheDocument();
  });
});
