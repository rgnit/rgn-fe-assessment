"use client";
import React, { useEffect, useState } from "react";
import { useTheme } from "styled-components";
import { ClipLoader } from "react-spinners";
import { Candidate } from "@/types/Candidate";
import { getCandidates } from "@/api/calls";
import { CandidatesListTable } from "@/components/CandidatesList/CandidatesList.styles";
import { DateTime } from "luxon";

type CandidatesListProps = {
  skillLevelFilter: Candidate["skill_level"] | "any";
};
export const CandidatesList = ({ skillLevelFilter }: CandidatesListProps) => {
  const [candidates, setCandidates] = useState<null | Array<Candidate>>(null);
  const theme = useTheme();

  useEffect(() => {
    (async () => {
      const candidates = await getCandidates();

      setCandidates(
        candidates.filter(
          (candidate) =>
            skillLevelFilter === "any" ||
            skillLevelFilter === candidate.skill_level,
        ),
      );
    })();
  }, []);

  const getCandidatesTable = () => {
    return (
      <CandidatesListTable>
        <thead>
          <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
            <th>Skill level</th>
            <th>Expertise</th>
            <th>City</th>
            <th>Country</th>
            <th>Hours per week</th>
          </tr>
        </thead>
        <tbody>
          {candidates?.map((candidate) => {
            return (
              <tr>
                <td>{candidate.first_name}</td>
                <td>{candidate.last_name}</td>
                <td>
                  {DateTime.fromMillis(candidate.date_of_birth).toFormat(
                    "dd LLLL yyyy",
                  )}
                </td>
                <td>{candidate.skill_level}</td>
                <td>{candidate.field_of_expertise}</td>
                <td>{candidate.city}</td>
                <td>{candidate.country}</td>
                <td>{candidate.preferred_hours_per_week}</td>
              </tr>
            );
          })}
        </tbody>
      </CandidatesListTable>
    );
  };

  if (candidates === null) {
    return <ClipLoader color={theme.colorPrimary} />;
  }
  return getCandidatesTable();
};
