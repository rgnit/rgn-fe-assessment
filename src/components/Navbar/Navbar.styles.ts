import styled from "styled-components";
export const navBarWidth = 250;
export const Navbar = styled.div`
  width: ${navBarWidth}px;
  height: 100vh;
  background-color: ${({ theme }) => theme.colorLightGray};
  position: fixed;
`;

export const LogoContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  padding-top: 16px;
`;

export const LinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 24px;
  padding: 32px;
  margin-top: 32px;

  a {
    color: ${({ theme }) => theme.colorFontNavbar};
    text-decoration: none;

    &:hover {
      color: ${({ theme }) => theme.colorFontNavbarHover};
    }
  }

  svg {
    margin-right: 8px;
  }
`;
