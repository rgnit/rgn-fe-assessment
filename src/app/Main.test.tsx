import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Home from "@/app/page";
import { renderWithRandstadTheme } from "@/lib/test-utils";

jest.mock("../components/VacancyHighlight/VacancyHighlight", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />;
    },
    VacancyHighlight: () => {
      return <div />;
    },
  };
});
describe("Home", () => {
  it("renders a heading", () => {
    renderWithRandstadTheme(<Home />);

    const heading = screen.getByRole("heading", {
      name: /Dashboard/i,
    });

    expect(heading).toBeInTheDocument();
  });
});
