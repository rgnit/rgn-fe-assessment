import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { renderWithRandstadTheme } from "@/lib/test-utils";
import { Navbar } from "@/components/Navbar/Navbar";

describe("Navbar", () => {
  it("shows all menu items", () => {
    renderWithRandstadTheme(<Navbar />);
    const navbar = screen.getByTestId("testNavbar");
    expect(navbar).toHaveTextContent("Dashboard");
    expect(navbar).toHaveTextContent("Candidates");
    expect(navbar).toHaveTextContent("Vacancies");
  });
});
