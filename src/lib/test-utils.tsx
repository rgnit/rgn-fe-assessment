import React, { FC, ReactElement } from "react";
import { ThemeProvider } from "styled-components";
import { render, RenderOptions } from "@testing-library/react";
import { RandstadTheme } from "@/themes/RandstadTheme";

const AllProviders = ({ children }: { children: ReactElement }) => {
  return <ThemeProvider theme={RandstadTheme}>{children}</ThemeProvider>;
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, "wrapper">,
) => render(ui, { wrapper: AllProviders, ...options });

export * from "@testing-library/react";
export { customRender as renderWithRandstadTheme };
