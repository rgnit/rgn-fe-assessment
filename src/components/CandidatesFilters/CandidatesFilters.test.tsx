import { screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { renderWithRandstadTheme } from "@/lib/test-utils";
import { CandidatesFilters } from "@/components/CandidatesFilters/CandidatesFilters";

describe("CandidatesFilters", () => {
  it("should have senior as filter option", () => {
    renderWithRandstadTheme(
      <CandidatesFilters setSkillLevelFilter={(value) => {}} />,
    );
    const combobox = screen.getByRole("combobox");
    expect(combobox).toHaveTextContent("senior");
  });
});
