"use client";
import React, { useState } from "react";
import { CandidatesList } from "@/components/CandidatesList/CandidatesList";
import { CandidatesFilters } from "@/components/CandidatesFilters/CandidatesFilters";
import { Candidate } from "@/types/Candidate";

export default function Page() {
  const [skillLevelFilter, setSkillLevelFilter] = useState<
    Candidate["skill_level"] | "any"
  >("any");
  return (
    <>
      <h1>Candidates</h1>
      <CandidatesFilters setSkillLevelFilter={setSkillLevelFilter} />
      <CandidatesList skillLevelFilter={skillLevelFilter} />
    </>
  );
}
