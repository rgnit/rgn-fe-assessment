import "styled-components";

export interface Theme {
  colorPrimary: string;
  colorWhite: string;
  colorLightGray: string;
  colorDarkGray: string;
  colorFontNavbar: string;
  colorFontNavbarHover: string;
  colorHightlightBackground: string;
  colorHighlightText: string;
  colorFont: string;
}

declare module "styled-components" {
  export interface DefaultTheme extends Theme {}
}
